# Telemetry Server Design Documentation

## Team Information
 * Team Members
   * Collin Bolles - cjb2849@rit.edu
   * Garnet Grimm  - gtg7470@rit.edu

## Introduction

### Purpose

The Telemetry Server aims to provide real time collection and storage of data collected
from the motorcycle. The goal of the project is to collect and push data from the
motorcycle to the webserver for data analysis.

### Glossary and Acronyms

|Term|Definition|
|------|---------|
| Bike Data | Any data that is collected from the motorcycle from the overseer |
| GUB | Gateway Utility Board |
| MQTT | Communication protocol used for communication between overseer and webserver |
| MVP | Minimum Viable Product |
| Overseer | Telemetry Server component running on Raspberry Pi on motorcycle |
| Web Server | Cloud based component of Telemetry Server |

## Requirements

The following include the basic requirements for the project as well as extra goals for
the project.

### Definition of MVP
The minimum viable product represent the basic requirements that are needed for the 
Telemetry Server to be successful and deployed in production. Failure to reach MVP will
result in the project not being used in production.

| Status | Title | Description |
|--------|-------|-------------|
| INCOMPLETE | SPI Data Collection | SPI interface to pull data from the GUB |
| INCOMPLETE | Overseer Data Pushing | Abstraction for sending bike data over MQTT |
| INCOMPLETE | Webserver Data Collection | Handlers on webserver to collect data sent over MQTT |
| INCOMPLETE | Data Storage | Storing data collected over MQTT into a database |
| INCOMPLETE | User Authentication | Handle user login/out into website |
| INCOMPLETE | Data Visualization | Visualization of collected data on the website |

### Enhancements
Enhancements are non-required functionality that may be a part of the Telemetry Server.
Enhancements are not required for the Telemetry Server to be used in production.

| Status | Title | Description |
|--------|-------|-------------|
| INCOMPLETE | Mobile Support | Data visualization and website support for mobile viewing|

## Application Domain

The Telemetry Server is physically constrained to two main sections. The Overseer
represents the component running on the motorcycle and the Webserver represents the cloud
computing component. The following diagram abstractly describes the separate aspects and 
how they interact with each other.

![Domain Model](resources/domain_model.png)

`Bike Data` is collected from the `GUB` by the `Overseer`. The `Bike Data` can represent
any data that is being collected from the motorcycle. The `Overseer` then sends the
`Bike Data` to the `Web Server`. The `Web Server` then stores the data into a
`Database`. The `Web Server` also has the ability to display the `Bike Data` on the 
`Website` which is accessible by the `User`.

## Architecture and Design

The architecture of the software mirrors the physical aspects of the project. Below are is
the outline for the physical outline of the project including the general flow of data
from motorcycle to database.

![Data Flow](resources/data_flow.png)

The `overseer` package contains all code that is running on the raspberry pi, code for
communication is used throughout the application so is part of a `common` package. The
`webserver` package contains all the code that is running on the server.

### Common

Contained in common is source which is shared between the Overseer and the Webserver.
Anything shared in terms of functionality or modeling is contained within the common
section.

#### Model

Within common is `model` which contains model object that are needed in both the webserver
and the overseer. This includes the protobuf messages which are sent over MQTT between
the webserver and the overseer.

#### MQTT

The MQTT package contains the logic for interacting with MQTT for both the overseer and
the webserver. This includes the functionality to send data over MQTT as well as the 
functionality to receive data over MQTT.

All payloads sent over MQTT are protobuf messages. In the `protobuf` folder are all of the
messages that can be sent over MQTT. In `config/message_topics.json` is a mapping between
the name of the message and the MQTT topic that the data is sent over.

##### Using the MQTT Sender

The sender works by taking in a protobuf message and sending the message over MQTT to the
topic that the message is related to as defined in `config/message_topics.json`. The
payload is the serialized version of the protobuf message.

##### Using the MQTT Receiver

The receiver contains a mapping between topics and `TopicHandler`'s. Each `TopicHandler`
has the ability to take in the serialized version of a protobuf message then does
additional functionality depending on the handler.

New `TopicHandlers` are used by extending the generic `TopicHandler` in the MQTT module.