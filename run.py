"""
Hanles execution of all commands related to the real time telemetry data
collection.

Commands
--------
generate_models:
    Converts protocol buffers to python classes in the model folder. Will
    replace the
    existing model files
"""
import os
import argparse
import subprocess
import telemetry.overseer.run as overseer

# The directory of this program
_CURRENT_FILE_DIR_LOCATION = os.path.abspath(os.path.dirname(__file__))
# The location of the python models
_MODEL_FOLDER_LOCATION = os.path.join(
    _CURRENT_FILE_DIR_LOCATION, 'telemetry/common/model/messages')
# The location where the protobuf messages  are
_PROTOBUF_FOLDER_LOCATION = os.path.join(_CURRENT_FILE_DIR_LOCATION, 'protobuf')
# The command used to generate the models
_PROTOC_COMMAND_TEMPLATE = 'protoc --proto_path={} --python_out={} {}'
# The commands that this run script can execute
_COMMANDS = ['generate_models', 'overseer']


def clear_folder(folder_path: str) -> None:
    """
    Clear the contents of a given folder excluding the __init__.py file

    folder_path (str): The path to the folder that should be cleared
    """
    for file_name in os.listdir(folder_path):
        file_path = os.path.join(folder_path, file_name)
        if '__init__' in file_name:
            os.remove(file_path)


def generate_models():
    """
    Handles generating the protocol buffers from the messages and replaces the existing
    models.
    """
    clear_folder(_MODEL_FOLDER_LOCATION)
    # Generate the models into the model folder
    protoc_command = _PROTOC_COMMAND_TEMPLATE.format(
        _PROTOBUF_FOLDER_LOCATION, _MODEL_FOLDER_LOCATION,
        os.path.join(_PROTOBUF_FOLDER_LOCATION, '*.proto'))
    subprocess.call(protoc_command, shell=True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Execute commands related to telemetry')
    parser.add_argument(
        'command', type=str, action='store', help='''
        The specific command to execute on the telemetry, commands include generate_models
        ''', choices=_COMMANDS)

    args = parser.parse_args()

    # Generate models
    if(args.command == _COMMANDS[0]):
        generate_models()
    # Run overseer
    elif(args.command == _COMMANDS[1]):
        overseer.begin_overseer()
