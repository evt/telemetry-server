# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConnectionState.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConnectionState.proto',
  package='',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\x15\x43onnectionState.proto\";\n\x0f\x43onnectionState\x12\x15\n\x05state\x18\x01 \x01(\x0e\x32\x06.State\x12\x11\n\ttimestamp\x18\x02 \x01(\x03*(\n\x05State\x12\r\n\tCONNECTED\x10\x00\x12\x10\n\x0c\x44ISCONNECTED\x10\x01\x62\x06proto3')
)

_STATE = _descriptor.EnumDescriptor(
  name='State',
  full_name='State',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='CONNECTED', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DISCONNECTED', index=1, number=1,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=86,
  serialized_end=126,
)
_sym_db.RegisterEnumDescriptor(_STATE)

State = enum_type_wrapper.EnumTypeWrapper(_STATE)
CONNECTED = 0
DISCONNECTED = 1



_CONNECTIONSTATE = _descriptor.Descriptor(
  name='ConnectionState',
  full_name='ConnectionState',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='state', full_name='ConnectionState.state', index=0,
      number=1, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='timestamp', full_name='ConnectionState.timestamp', index=1,
      number=2, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=25,
  serialized_end=84,
)

_CONNECTIONSTATE.fields_by_name['state'].enum_type = _STATE
DESCRIPTOR.message_types_by_name['ConnectionState'] = _CONNECTIONSTATE
DESCRIPTOR.enum_types_by_name['State'] = _STATE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConnectionState = _reflection.GeneratedProtocolMessageType('ConnectionState', (_message.Message,), {
  'DESCRIPTOR' : _CONNECTIONSTATE,
  '__module__' : 'ConnectionState_pb2'
  # @@protoc_insertion_point(class_scope:ConnectionState)
  })
_sym_db.RegisterMessage(ConnectionState)


# @@protoc_insertion_point(module_scope)
