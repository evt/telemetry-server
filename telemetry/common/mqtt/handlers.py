from telemetry.common.model.messages.BMSTemp_pb2 import BMSTemp
from telemetry.common.model.messages.ConnectionState_pb2 import ConnectionState, State
from abc import ABC, abstractclassmethod


class TopicHandler (ABC):
    def __init__(self, topic: str):
        """
        A general handler for messages that come in for a specific topic. Each topic
        handler specifies which topic that should be subscribed to and which function
        should run after a message is passed to the  given topic

        topic (str): The topic that should be subscribed to
        """
        self.topic = topic

    @abstractclassmethod
    def on_message(self, topic: str, raw_data: str):
        """
        Every on message method will be passed the topic that triggered the method call
        and the raw data of the message body as a string. There are no other expectations
        for the method

        topic (str): The topic that caused the method to be called
        raw_data (str): The data found in the message contents
        """
        pass


class BMSTempHandler (TopicHandler):
    def __init__(self, topic: str):
        """
        Handles messages being made related to the BMS temperature data.

        topic (str): The topic on which BMS temp data will be coming in on

        TODO:
            * Add real data collection rather then the current test code
        """
        super(BMSTempHandler, self).__init__(topic)

    def on_message(self, topic: str, raw_data: str):
        """
        Parses in the raw data into a BMS Temp data object and prints out the results

        topic (str): The MQTT topic that the data was posted under
        raw_data (str): The serialized representation of the BMS temp data
        """
        bms_temp = BMSTemp()
        bms_temp.ParseFromString(raw_data)
        print(bms_temp.slaves[0].num_cells)


class ConnectionHandler (TopicHandler):
    def __init__(self, topic: str):
        """
        Handles keeping track of the connection state of the overseer. Ensures that the
        overseer has not timedout and is still sending information. Keeps reading the
        connection state checking for the time from the last connection status or a
        disconnect message.

        topic (str): The MQTT topic that the connection status is updated on
        is_connected (bool): State of connection to the overseer
        """
        super(ConnectionHandler, self).__init__(topic)
        self.is_connected = False

    def on_message(self, topic: str, raw_data: str):
        """
        When the message is received, the connection status is checked as well as the
        timeout state
        """
        connection_state = ConnectionState()
        connection_state.ParseFromString(raw_data)
        self.is_connected = connection_state.state == State.CONNECTED
        print(is_connected)
