"""
API for sending and receiving data across MQTT. Sends the serialized representation
of a model over MQTT to a specific topic that can then be re-read
"""
from google.protobuf.message import Message
import paho.mqtt.client as paho
from telemetry.common.mqtt.handlers import TopicHandler
from typing import Dict


class MQTTSender:
    def __init__(self, client: paho.Client, topic_relation: Dict[str, str]):
        """
        Creates an MQTT client based on the provided host.

        client (paho.Client): The client to use to send data over MQTT, assumed to be connected
        topic_relation (Dict[str, str]): A mapping between class names and the topic to send them to
        """
        self.client = client
        self.topic_relation = topic_relation

    def send(self, data: Message) -> None:
        """
        Sends the protobuf message over MQTT to a specific topic. The topic is determined by pulling the topic
        from the topic_relation that was originally passed in.

        data (str): The information stored in a protobuf to send over MQTT.
        """
        raw_data = data.SerializeToString()
        topic = self.topic_relation[data.__class__.__name__]
        self.client.publish(topic, raw_data)


class MQTTReceiver:
    def __init__(self, client: paho.Client):
        """
        Listens to messages coming in through MQTT parses the data back into a usable
        python class, then calls the function associated with the data.

        client (paho.Client): The MQTT client to communicate with
        topic_relation (Dict[str, str]): The mapping between message name and topic
        """
        self.client = client
        self.call_backs = dict()

        def on_message(client: paho.Client, userdata, message: paho.MQTTMessage):
            """
            When a message is received on one of the subscribed to topics, the function
            to call is found in the call back look up table and the message payload is
            passed in as well as the topic

            client (paho.Client): The client that received the message
            userdata: Predefined user data for on message received
            message (paho.MQTTMessage): The message received over MQTT
            """
            topic = message.topic
            self.call_backs[topic](topic, message.payload)

        self.client.on_message = on_message

    def subscribe(self, topic_handler: TopicHandler):
        """
        Subscribe to a specific topic on which each time a message is received the
        call_back function is called

        topic_handler (TopicHandler): The handler that contains a method that will be
            called each time the associated topic is published to
        """
        self.client.subscribe(topic_handler.topic)
        self.call_backs[topic_handler.topic] = topic_handler.on_message
