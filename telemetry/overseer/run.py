"""
Represents the entry point for the overseer application. Begins a process for polling data
from the GLUB and passing the information over MQTT to the webserver
"""
import os
import json
from configparser import ConfigParser
import paho.mqtt.client as paho
import telemetry.common.mqtt.mqtt as mqtt
import telemetry.common.mqtt.handlers as handlers
from telemetry.common.model.messages.BMSTemp_pb2 import BMSTemp, LTC6811
from telemetry.common.model.messages.ConnectionState_pb2 import ConnectionState, State
from time import sleep
import time


def begin_overseer():
    """
    Function to call to start the overseer communication with the GLUB and MQTT server
    """
    config_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../config')

    # Read in message to topic mapping
    topic_relation_file_path = os.path.join(config_dir, 'message_topics.json')
    with open(topic_relation_file_path, 'r') as topic_file:
        topic_relation = json.load(topic_file)

    # Read in application configuration
    application_settings_path = os.path.join(config_dir, 'application_settings.ini')
    application_settings = ConfigParser()
    application_settings.read(application_settings_path)

    # Create MQTT Client
    mqtt_settings = application_settings['MQTT']
    mqtt_client = paho.Client()
    mqtt_client.connect(mqtt_settings['hostname'], int(mqtt_settings['port']))

    # Create MQTT Sender
    mqtt_sender = mqtt.MQTTSender(mqtt_client, topic_relation)

    # Send connection message
    connection_state = ConnectionState()
    connection_state.state = State.CONNECTED
    connection_state.timestamp = int(time.time())
    mqtt_sender.send(connection_state)

    # Create receiver for testing
    bms_temp_handler = handlers.BMSTempHandler(topic_relation['BMSTemp'])
    mqtt_receiver = mqtt.MQTTReceiver(mqtt_client)
    mqtt_receiver.subscribe(bms_temp_handler)

    mqtt_client.loop_start()

    # Test loop, send random temp data over MQTT
    for i in range(0, 3):
        # Create test data
        ltc8611_slaves = []
        for i in range(0, 9):
            ltc8611 = LTC6811()
            ltc8611.num_cells = 2
            ltc8611.cell_voltages.extend([1, 2])
            ltc8611.aux_voltages.extend([2, 3])
            ltc8611_slaves.append(ltc8611)
        bms_temp = BMSTemp()
        bms_temp.num_slaves = 9
        bms_temp.slaves.extend(ltc8611_slaves)

        # Send test data
        mqtt_sender.send(bms_temp)

        # Resend connection state
        connection_state.timestamp = int(time.time())
        mqtt_sender.send(connection_state)

        sleep(2)

    # Send disconnect message
    connection_state.state = State.DISCONNECTED
    connection_state.timestamp = int(time.time())
    mqtt_sender.send(connection_state)
