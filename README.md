# telemetry-server

## Table of Contents
- [telemetry-server](#telemetry-server)
  - [Table of Contents](#table-of-contents)
  - [Introduction](#introduction)
    - [Overseer](#overseer)
    - [Webserver](#webserver)
  - [Getting Started](#getting-started)
  - [Using the Run Script](#using-the-run-script)
    - [Generating Models](#generating-models)
    - [Running the Overseer](#running-the-overseer)
      - [Testing Overseer](#testing-overseer)

## Introduction
The Telemetry Server project has the goal of having pseudo-real time analysis of the
rev 2 motorcycle during use. The data from the motorcycle will be transmitted from the
motorcycle to a web server for processing and team access. 

There are two main components to the telemetry server project. First is the "overseer"
which handles collecting data from the motorcycle and posting the information to a MQTT
broker. Second is the webserver which listens to the MQTT broker and processes the data.

### Overseer
The overseer runs on the motorcycle collecting data from the GLUB then pushing the data
to an MQTT broker. The overseer communicates with the GLUB through SPI, and uses an LTE
model to send the data over MQTT.

### Webserver
The webserver runs remotely and handles advanced data processing of the information
provided by the overseer. The information is retrieved by listening to specific topics
on the MQTT broker.

## Getting Started

1. Installing Protoc
   
    Protoc is used for compiling the protobuf messages into usable python code that can
    be used on both the overseer and the webserver.

    Follow instructions [at this website](http://google.github.io/proto-lens/installing-protoc.html) to install protoc

2. Setting Up Virtual Environment
   
    Setting up either virtual-environment or anaconda is recommended for the project.

    Follow instructions [at this website](https://www.geeksforgeeks.org/python-virtual-environment/) to setup a virtual environment.

3. Updating PYTHONPATH

    Update PYTHONPATH to include the source of the project. This can be achieved by
    updating the activate script in virtual environment.

    Add the following line to the top of the activate script in 
    ```
    <path to venv>/bin/activate
    ```
    ```
    export PYTHONPATH="$PYTHONPATH:<path to source>/telemetry-server/"
    ```

4. Install requirements

    Installing the requirements through pip

    ```
    pip install -r requirements.txt
    ```

## Using the Run Script

### Generating Models
Running the following command will generate the model files from the protobuf messages
into python source

```
python run.py generate_models
```

### Running the Overseer
Running the following command will start the overseer communication over MQTT.

```
python run.py overseer
```

Configuration for the information on the MQTT broker is set in 
`config/application_settings.ini`

#### Testing Overseer
1. Install Mosquitto Broker
    Follow instructions at [this link](https://mosquitto.org/download/)

2. Starting Mosquitto Broker

    On Mac

    ```
    /usr/local/sbin/mosquitto -c /usr/local/etc/mosquitto/mosquitto.conf
    ```
    On Linux

    ```
    mosquitto -v
    ```

3. Creating Subscriber
   
    To create a test subscriber to listen for incoming messages run the following
    command

    ```
    mosquitto_sub -h localhost -p 1883 -t <TOPIC>
    ```
    
    where topic is the topic to listen for. This depends on what overseer is producing.